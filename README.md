# 5. Politiques de réseau

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Utilisation des Politiques Réseau dans Kubernetes

Nous allons parler de l'utilisation des politiques réseau (network policies). Voici un aperçu rapide de ce que nous allons aborder :

- Qu'est-ce qu'une politique réseau ?
- Le concept de sélecteur de pods dans une politique réseau
- Les notions de trafic entrant (ingress) et sortant (egress)
- Les sélecteurs "from" et "to"
- Les ports
- Une démonstration pratique

# Qu'est-ce qu'une politique réseau ?

Une politique réseau est un objet qui permet de contrôler le flux de communication réseau vers et depuis vos pods. Cela vous permet de créer un réseau de cluster plus sécurisé en isolant les pods du trafic dont ils n'ont pas besoin.

# Sélecteur de Pods

Le sélecteur de pods détermine à quels pods dans le namespace la politique réseau s'applique. Le sélecteur de pods peut sélectionner les pods en utilisant des labels. Voici à quoi ressemble un sélecteur de pods. Nous avons une politique réseau de base, et cette politique réseau s'appliquera à tous les pods dans le même namespace que la politique, qui ont ce label `role=db`. Par défaut, les pods sont considérés comme non isolés et complètement ouverts à toute communication. Mais dès qu'une politique réseau sélectionne un pod en utilisant son sélecteur de pods, le pod est alors considéré comme isolé et ne sera ouvert qu'au trafic explicitement autorisé par les politiques réseau.

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: my-network-policy
spec:
  podSelector: #<-- Début du Selector 
    metchLabels:
      role: db
```

# Trafic entrant (ingress) et sortant (egress)

Une politique réseau peut s'appliquer au trafic entrant, au trafic sortant ou aux deux types de trafic. Le trafic entrant (ingress) désigne le trafic réseau entrant, qui provient d'une source extérieure au pod. Le trafic sortant (egress) est le trafic réseau sortant, qui quitte le pod vers une autre destination. Essentiellement, les politiques réseau peuvent contrôler le trafic entrant ou sortant d'un pod, ou les deux.

# Sélecteurs "From" et "To" Selectors

Les sélecteurs "from" et "to" sont liés aux notions d'ingress et d'egress, et ils déterminent quel trafic est autorisé par la politique réseau. Le sélecteur "from" sélectionne le trafic entrant autorisé. Le sélecteur "to" fait de même pour le trafic sortant autorisé.

```yaml
spec:
  ingress:
    - from:
    ...
  egress:
    - to:
    ...
```

Exemple : 

**podSelector**

Select Pods to allow traffic from/to

```yaml
spec:
  ingress:
    - from:
      - podSelector:
        matchLabels:
          app: db
```

**namespaceSelector**

Select namespace to allow traffic from/to

```yaml
spec:
  ingress:
    - from:
      - namspaceSelector:
        matchLabels:
          app: db
```

**ipBlock**

Select an IP range to allow traffic from/to

```yaml
spec:
  ingress:
    - from:
      - ipBlock:
          app: 172.17.0.0/16
```


# Ports

Les ports spécifient un ou plusieurs ports qui autoriseront le trafic. Voici une règle d'ingress de base. Nous avons notre sélecteur "from", mais nous avons également spécifié un port. Nous spécifions le port 80, ce qui détermine quels ports seront autorisés pour ce trafic. Le trafic n'est autorisé que s'il correspond à la fois à un port autorisé et à une règle "from/to" autorisée.

```yaml
spec:
  ingress:
    - from:
      ports:
        - protocol: TCP
          port: 80
```

# Démonstration Pratique

Plongeons dans une démonstration pratique pour voir comment ces concepts fonctionnent dans notre cluster Kubernetes.

1. Connectez-vous à votre nœud de contrôle Kubernetes :

```bash
ssh -i id_rsa user@PUBLIC_IP_ADDRESS
```

2. Créez un nouveau namespace pour organiser les tests des politiques réseau :

```bash
kubectl create namespace np-test
```

3. Ajoutez un label à ce namespace :

```bash
kubectl label namespace np-test team=np-test
```

4. Créez un pod serveur web simple (nginx) dans le namespace `np-test` :

```yaml
nano np-nginx.yml
```

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: np-nginx
     labels:
       app: nginx
   spec:
     containers:
     - name: nginx
       image: nginx
   ```

   ```bash
   kubectl apply -f np-nginx.yml -n np-test
   ```

5. Créez un pod client (busybox) dans le namespace `np-test` :
```yaml
nano np-busybox.yml
```

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: np-busybox
     labels:
       app: client
   spec:
     containers:
     - name: busybox
       image: radial/busyboxplus:curl
       command: ['sh', '-c', 'while true; do sleep 5; done']
   ```

   ```bash
   kubectl apply -f np-busybox.yml -n np-test
   ```

6. Obtenez l'adresse IP du pod `np-nginx` :
   ```bash
   kubectl get pods -o wide -n np-test
   ```

7. Tentez de communiquer avec le pod `nginx` à partir du pod `busybox` :
   ```bash
   kubectl exec -it np-busybox -n np-test -- curl <IP_ADDRESS_NGINX>
   ```

8. Créez une politique réseau qui sélectionne le pod `nginx` :

```yaml
nano my-networkpolicy.yml
```

   ```yaml
   apiVersion: networking.k8s.io/v1
   kind: NetworkPolicy
   metadata:
     name: deny-all
     namespace: np-test
   spec:
     podSelector:
       matchLabels:
         app: nginx
     policyTypes:
     - Ingress
     - Egress
   ```

```bash
kubectl apply -f my-networkpolicy.yml
```

9. Essayez de nouveau de communiquer avec le pod `nginx` à partir du pod `busybox` :

```bash
kubectl exec -it np-busybox -n np-test -- curl <IP_ADDRESS_NGINX>
```

10. Modifiez la politique réseau pour autoriser le trafic entrant du namespace `np-test` :

```yaml
nano my-networkpolicy.yml
```

    ```yaml
    apiVersion: networking.k8s.io/v1
    kind: NetworkPolicy
    metadata:
      name: allow-np-test
      namespace: np-test
    spec:
      podSelector:
        matchLabels:
          app: nginx
      policyTypes:
      - Ingress
      - Egress
      ingress:
      - from:
        - namespaceSelector:
            matchLabels:
              team: np-test
        ports:
        - protocol: TCP
          port: 80
    ```

    ```bash
    kubectl apply -f my-networkpolicy.yml
    ```

11. Testez de nouveau la communication avec le pod `nginx` :

```bash
kubectl exec -it np-busybox -n np-test -- curl <IP_ADDRESS_NGINX>
```

# Conclusion

Nous avons parlé de ce qu'est une politique réseau, des sélecteurs de pods, des notions de trafic entrant et sortant, des sélecteurs "from" et "to", des ports, et nous avons réalisé une démonstration pratique. C'est tout pour cette leçon. À la prochaine !




# Reférences

https://kubernetes.io/docs/concepts/services-networking/network-policies/
